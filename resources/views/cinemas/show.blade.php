<x-app>
  <x-slot name="page_title">{{ $cinema->name }}</x-slot>

  @can('create')
    <a href="{{ route('cinema.create') }}"
      class="align-left focus:outline-none text-white text-sm py-2.5 px-5 rounded-md bg-green-500 hover:bg-green-600 hover:shadow-lg">
      {{ __('New') }}
    </a>
  @endcan

  <table class="divide-y divide-gray-200 mt-4">
    <thead class="bg-gray-50">
      <tr>
        <th class="text-left px-2 py-3">{{ __('Room id') }}</th>
        <th class="text-left px-2 py-3">{{ __('Room places') }}</th>
        <th class="text-left px-2 py-3">{{ __('Sessions') }}</th>
      </tr>
    </thead>
    <tbody class="bg-white text-xs divide-y divide-gray-200">
      @foreach ($cinema->rooms as $room)
        <tr>
          <td class="px-2 py-4">{{ $room->id }}</td>
          <td class="px-2 py-4">{{ $room->places }}</td>
          <td class="px-2 py-4">
            @foreach ($room->sessions as $session)
              <div>
                {{ $session->movie->title . ' | ' . $session->begin }}
              </div>
            @endforeach
          </td>

        </tr>
      @endforeach
    </tbody>
  </table>
</x-app>
