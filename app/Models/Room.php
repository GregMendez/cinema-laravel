<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
	use HasFactory;

	protected $fillable = ['places', 'cinema_id', 'user_id'];

	public function cinema()
	{
		return $this->belongsTo(Cinema::class);
	}

  public function sessions()
  {
    return $this->hasMany(Session::class);
  }
}
