<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cinema extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'address_line', 'city', 'country_id', 'user_id'];

    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
