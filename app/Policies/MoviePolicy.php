<?php

namespace App\Policies;

use App\Models\Artist;
use App\Models\Movie;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MoviePolicy
{
  use HandlesAuthorization;

  /**
   * Determine whether the user can view any models.
   *
   * @param  \App\Models\User  $user
   * @return mixed
   */
  public function viewAny(User $user)
  {
    //
  }

  /**
   * Determine whether the user can view the model.
   *
   * @param  \App\Models\User  $user
   * @param  \App\Models\Movie  $movie
   * @return mixed
   */
  public function view(User $user, Movie $movie)
  {
    //
  }

  /**
   * Determine whether the user can create models.
   *
   * @param  \App\Models\User  $user
   * @return mixed
   */
  public function create(User $user)
  {
    return $user ? true : false;
  }

  /**
   * Determine whether the user can update the model.
   *
   * @param  \App\Models\User  $user
   * @param  \App\Models\Movie  $movie
   * @return mixed
   */
  public function update(User $user, Movie $movie)
  {
    return $user->id === $movie->user_id;
  }

  /**
   * Determine whether the user can delete the model.
   *
   * @param  \App\Models\User  $user
   * @param  \App\Models\Movie  $movie
   * @return mixed
   */
  public function delete(User $user, Movie $movie)
  {
    return $user->id === $movie->user_id;
  }

  /**
   * Determine whether the user can restore the model.
   *
   * @param  \App\Models\User  $user
   * @param  \App\Models\Movie  $movie
   * @return mixed
   */
  public function restore(User $user, Movie $movie)
  {
    return $user->id === $movie->user_id;
  }

  /**
   * Determine whether the user can permanently delete the model.
   *
   * @param  \App\Models\User  $user
   * @param  \App\Models\Movie  $movie
   * @return mixed
   */
  public function forceDelete(User $user, Movie $movie)
  {
    return $user->id === $movie->user_id;
  }

  /**
   * @param  \App\Models\User  $user
   * @param  \App\Models\Artist  $movie
   * @return mixed
   */
  public function attach(User $user, Movie $movie)
  {
    return $user->id === $movie->user_id;
  }

  /**
   * @param  \App\Models\User  $user
   * @param  \App\Models\Movie $movie
   * @param  \App\Models\Artist $artist
   * @return mixed
   */
  public function detach(User $user, Movie $movie)
  {
    return $user->id === $movie->user_id;
  }
}
