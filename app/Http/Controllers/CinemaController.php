<?php

namespace App\Http\Controllers;

use App\Http\Requests\CinemaRequest;
use App\Models\Cinema;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CinemaController extends Controller
{
  /**
   * Class constructor
   */
  public function __construct()
  {
    $this->middleware('ajax')->only('destroy');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('cinemas.index', ['cinemas' => Cinema::paginate(10), 'cinema' => Cinema::class]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $this->authorize('create', Cinema::class);

    return view('cinemas.create', ['countries' => Country::all()]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(CinemaRequest $request)
  {
    $data = $request->all();
    $data['user_id'] = Auth::user()->id;

    Cinema::create($data);

    return redirect()->route('cinema.index')
      ->with('ok', __('Cinema has been saved !'));
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(Cinema $cinema)
  {
    return view('cinemas.show', [
      'cinema' => $cinema
    ]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit(Cinema $cinema)
  {
    $this->authorize('update', $cinema);

    return view('cinemas.edit', [
      'cinema' => $cinema,
      'countries' => Country::all()
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(CinemaRequest $request, Cinema $cinema)
  {
    $this->authorize('update', $cinema);

    $cinema->update($request->all());

    return redirect()->route('cinema.index')
      ->with('ok', __('Cinema has been updated !'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Cinema $cinema)
  {
    $cinema->delete();

    return response()->json();
  }
}
