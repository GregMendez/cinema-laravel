<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoomRequest;
use App\Models\Cinema;
use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Whoops\Run;

class RoomController extends Controller
{
  /**
   * Class constructor
   */
  public function __construct()
  {
    $this->middleware('ajax')->only('destroy');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('rooms.index', ['rooms' => Room::paginate(10), 'room' => Room::class]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $this->authorize('create', Room::class);

    return view('rooms.create', ['cinemas' => Cinema::all()]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(RoomRequest $request)
  {
    $data = $request->all();
    $data['user_id'] = Auth::user()->id;

    Room::create($data);

    return redirect()->route('room.index')
      ->with('ok', __('Room has been saved !'));
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit(Room $room)
  {
    $this->authorize('update', $room);

    return view('rooms.edit', [
      'room' => $room,
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(RoomRequest $request, Room $room)
  {
    $this->authorize('update', $room);

    $room->update($request->all());

    return redirect()->route('room.index')
      ->with('ok', __('Room has been updated !'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Room $room)
  {
    $room->delete();

    return response()->json();
  }
}
