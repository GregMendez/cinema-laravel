<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CinemasTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('cinemas')->insert([
      [
        'name' => 'Pathé Omar 1',
        'address_line' => '2948  Holden Street',
        'city' => 'San Diego',
        'country_id' => 1,
      ], [
        'name' => 'Pathé Omar 2',
        'address_line' => '4153  Argonne Street',
        'city' => 'FORT WORTH',
        'country_id' => 1,
      ], [
        'name' => 'Pathé Omar 3',
        'address_line' => '3059  Rosemont Avenue',
        'city' => 'Orlando',
        'country_id' => 1,
      ], [
        'name' => 'Pathé Omar 4',
        'address_line' => '58  Columbia Boulevard',
        'city' => 'Hanover',
        'country_id' => 1,
      ], [
        'name' => 'Pathé Omar 5',
        'address_line' => 'Hauptstrasse 76',
        'city' => 'Montignez',
        'country_id' => 3,
      ], [
        'name' => 'Pathé Omar 6',
        'address_line' => '130  rue Porte d',
        'city' => 'CAVAILLON',
        'country_id' => 4,
      ]
    ]);
  }
}
