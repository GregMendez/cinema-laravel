<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SessionsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('sessions')->insert([
      [
        'begin' => '2021-03-10, 17:16:18',
        'room_id' => 1,
        'movie_id' => 3
      ], [
        'begin' => '2020-03-10 17:16:18',
        'room_id' => 5,
        'movie_id' => 4
      ], [
        'begin' => '2019-03-10 17:16:18',
        'room_id' => 1,
        'movie_id' => 1
      ], [
        'begin' => '2018-03-10 17:16:18',
        'room_id' => 3,
        'movie_id' => 2
      ], [
        'begin' => '2017-03-10 17:16:18',
        'room_id' => 2,
        'movie_id' => 2
      ],
    ]);
  }
}
